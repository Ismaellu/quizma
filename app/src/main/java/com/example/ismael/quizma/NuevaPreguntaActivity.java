package com.example.ismael.quizma;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class NuevaPreguntaActivity extends AppCompatActivity {

    final private int CODE_WRITE_EXTERNAL_STORAGE_PERMISSION = 123;
    final private int CODE_WRITE_EXTERNAL_STORAGE_PERMISSION_CAMERA = 456;
    final private int CODE_WRITE_EXTERNAL_STORAGE_PERMISSION_GALERY = 789;
    private Context myContext;
    private ConstraintLayout constraintLayoutNuevaPreguntaActivity;
    private ArrayAdapter<String> adapter;

    private EditText titulo;
    private Spinner spinnerCategoria;
    private EditText respuestaCorrecta;
    private EditText respuestaIncorrecta1;
    private EditText respuestaIncorrecta2;
    private EditText respuestaIncorrecta3;
    private ImageView imagen;
    private Button crear;
    private Button addCategoria;
    private Button buttonBorrar;
    private Button buttonTakePhoto;
    private Button buttonAddPhoto;
    private Button borrarImagen;
    private String imagen64;
    final String pathFotos = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/cameraImages/";
    private Uri uri;
    private static final int REQUEST_CAPTURE_IMAGE = 200;
    private static final int REQUEST_SELECT_IMAGE = 201;

    private String TAG = "NuevaPreguntaActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_pregunta);
        //Esto que hay encima tiene que estar siempre

        // Almacenamos el contexto de la actividad para utilizar en las clases internas
        myContext = this;

        // Recuperamos el Layout donde mostrar el Snackbar con las notificaciones

        constraintLayoutNuevaPreguntaActivity = findViewById(R.id.constraintLayoutNuevaPreguntaActivity);
        titulo = (EditText) findViewById(R.id.editTextNuevaPregunta);
        spinnerCategoria = (Spinner) findViewById(R.id.spinnerNuevaPregunta);
        respuestaCorrecta = (EditText) findViewById(R.id.editTextRespuestaCorrecta);
        respuestaIncorrecta1 = (EditText) findViewById(R.id.editTextRespuestaIncorrecta1);
        respuestaIncorrecta2 = (EditText) findViewById(R.id.editTextRespuestaIncorrecta2);
        respuestaIncorrecta3 = (EditText) findViewById(R.id.editTextRespuestaIncorrecta3);
        crear = (Button) findViewById(R.id.buttonCrearNuevaPregunta);
        addCategoria = (Button) findViewById(R.id.buttonNuevaCategoria);
        buttonBorrar = (Button) findViewById(R.id.buttonBorrar);
        buttonTakePhoto = (Button) findViewById(R.id.buttonTakePhoto);
        buttonAddPhoto = (Button) findViewById(R.id.buttonAddPhoto);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarNuevaPregunta);
        imagen = (ImageView) findViewById(R.id.imageViewPregunta);
        borrarImagen = (Button) findViewById(R.id.borrarImagen);

        // Definición de la lista de categorias
        final ArrayList<String> itemsCategoria = Repositorio.consultarCategoria(myContext);

        //Metodo para añadir una categoria
        addCategoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                // Recuperación de la vista del AlertDialog a partir del layout de la Actividad
                LayoutInflater layoutActivity = LayoutInflater.from(myContext);
                View viewAlertDialog = layoutActivity.inflate(R.layout.alert_dialog, null);

                // Definición del AlertDialog
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(myContext);

                // Asignación del AlertDialog a su vista
                alertDialog.setView(viewAlertDialog);

                // Recuperación del EditText del AlertDialog
                final EditText dialogInput = (EditText) viewAlertDialog.findViewById(R.id.dialogInput);

                // Configuración del AlertDialog
                alertDialog
                        .setCancelable(false)
                        // Botón Añadir
                        .setPositiveButton(getResources().getString(R.string.add),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        if (dialogInput.getText().toString().trim().isEmpty()) {
                                            Snackbar.make(view, getResources().getText(R.string.snackBarCategoriaVacio), Snackbar.LENGTH_LONG)
                                                    .setAction("Action", null).show();
                                        } else {
                                            adapter.add(dialogInput.getText().toString());
                                            spinnerCategoria.setSelection(adapter.getPosition(dialogInput.getText().toString()));
                                        }

                                    }
                                })
                        // Botón Cancelar
                        .setNegativeButton(getResources().getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                })
                        .create()
                        .show();
            }
        });


        // Definición del Adaptador que contiene la lista de categorias
        adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, itemsCategoria);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        // Definición del Spinner
        spinnerCategoria = (Spinner) findViewById(R.id.spinnerNuevaPregunta);
        spinnerCategoria.setAdapter(adapter);

        final Intent startingIntent = getIntent();


        //Rellenar NuevaPregunta con los datos que hemos obtenido
        if (startingIntent.hasExtra("id")) {

            int idRecuperada = getIntent().getExtras().getInt("id");

            Pregunta preguntaRecuperada = Repositorio.recuperarPregunta(idRecuperada, myContext);

            titulo.setText(preguntaRecuperada.getTitulo());
            spinnerCategoria.setSelection(adapter.getPosition(preguntaRecuperada.getCategoria()));
            respuestaCorrecta.setText(preguntaRecuperada.getRespuestaCorrecta());
            respuestaIncorrecta1.setText(preguntaRecuperada.getRepuestaIncorrecta1());
            respuestaIncorrecta2.setText(preguntaRecuperada.getRepuestaIncorrecta2());
            respuestaIncorrecta3.setText(preguntaRecuperada.getRepuestaIncorrecta3());
            imagen64 = preguntaRecuperada.getImagen64();

            byte [] codigoImagen = Base64.decode(imagen64, Base64.DEFAULT);
            Bitmap imagenDecoded = BitmapFactory.decodeByteArray(codigoImagen, 0, codigoImagen.length);

            imagen.setImageBitmap(imagenDecoded);


        }
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_nueva_pregunta));
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Asignar la acción necesaria. En este caso "volver atrás"
                    onBackPressed();
                }
            });
        } else {

        }

        //Click de la camara
        if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            buttonTakePhoto.setEnabled(false);
        } else {
            buttonTakePhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(crear.getWindowToken(), 0);

                    int WriteExternalStoragePermission = ContextCompat.checkSelfPermission(myContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    MyLog.d("ModificacionPreguntasActivity", "WRITE_EXTERNAL_STORAGE Permission: " + WriteExternalStoragePermission);


                    if (WriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
                        // Permiso denegado
                        // A partir de Marshmallow (6.0) se pide aceptar o rechazar el permiso en tiempo de ejecución
                        // En las versiones anteriores no es posibse ejecuta el método "onRequestPermissionsResult" para manejar la respuesta
                        //                            // Si el usuario marca "No preguntar más" no se volverá a mle hacerlo
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(NuevaPreguntaActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CODE_WRITE_EXTERNAL_STORAGE_PERMISSION_CAMERA);
                            // Una vez que se pide aceptar o rechazar el permiso mostrar este diálogo
                        } else {
                            Snackbar.make(constraintLayoutNuevaPreguntaActivity, getResources().getString(R.string.permisoEscrituraGarantizado), Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } else {
                        if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
                            takePicture();
                        }
                    }
                }
            });
        }

        //Elegir imagen de la galeria
        if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            buttonAddPhoto.setEnabled(false);
        } else {
            buttonAddPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(crear.getWindowToken(), 0);

                    int WriteExternalStoragePermission = ContextCompat.checkSelfPermission(myContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    MyLog.d("ModificacionPreguntasActivity", "WRITE_EXTERNAL_STORAGE Permission: " + WriteExternalStoragePermission);


                    if (WriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
                        // Permiso denegado
                        // A partir de Marshmallow (6.0) se pide aceptar o rechazar el permiso en tiempo de ejecución
                        // En las versiones anteriores no es posibse ejecuta el método "onRequestPermissionsResult" para manejar la respuesta
                        //                            // Si el usuario marca "No preguntar más" no se volverá a mle hacerlo
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(NuevaPreguntaActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CODE_WRITE_EXTERNAL_STORAGE_PERMISSION_GALERY);
                            // Una vez que se pide aceptar o rechazar el permiso mostrar este diálogo
                        } else {
                            Snackbar.make(constraintLayoutNuevaPreguntaActivity, getResources().getString(R.string.permisoEscrituraGarantizado), Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } else {
                        if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
                            selectPicture();
                        }
                    }
                }
            });
        }


        buttonBorrar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {
                // Recuperación de la vista del AlertDialog a partir del layout de la Actividad
                LayoutInflater layoutActivity = LayoutInflater.from(myContext);
                final View viewAlertDialog = layoutActivity.inflate(R.layout.alert_dialog_borrar, null);

                // Definición del AlertDialog
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(myContext);

                // Asignación del AlertDialog a su vista
                alertDialog.setView(viewAlertDialog);

                // Recuperación del EditText del AlertDialog
                final EditText dialogInput = (EditText) viewAlertDialog.findViewById(R.id.dialogInput);

                // Configuración del AlertDialog
                alertDialog.setCancelable(false)
                        // Botón Aceptar
                        .setPositiveButton(getResources().getString(R.string.botonAceptar),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        if (startingIntent.hasExtra("id")) { // comprobamos si venimos de edit
                                            int idRecogido = getIntent().getExtras().getInt("id");

                                            Repositorio.eliminarPregunta(myContext, idRecogido);
                                            Toast.makeText(myContext, "Pregunta eliminada correctamente", Toast.LENGTH_SHORT).show();
                                            finish();
                                        } else {
                                            Toast.makeText(myContext, "Esta pregunta todavía no ha sido creada", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                        // Botón Cancelar
                        .setNegativeButton(getResources().getString(R.string.botonCancelar),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                })
                        .create()
                        .show();
            }

        });

        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { //aqui se ejecuta el codigo al pulsar el boton

                /**
                 * Con estos if y elseif lo que comprobamos es que los campos de la nueva pregunta
                 * no se encuentren vacios y en el caso de que intentes crear una nueva
                 * aparezca una snackbar indicandote que los campos estan vacios
                 */
                if (titulo.getText().toString().isEmpty()) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(crear.getWindowToken(), 0);
                    //con el metodo getResources podemos coger una cadena del string como si hicieramos un @string/enunciadovacio
                    Snackbar.make(view, getResources().getText(R.string.snackBarEnunciadoVacio), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                } else if (itemsCategoria.isEmpty()) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(crear.getWindowToken(), 0);
                    Snackbar.make(view, getResources().getText(R.string.snackBarCategoriaVacio), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else if (respuestaCorrecta.getText().toString().isEmpty()) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(crear.getWindowToken(), 0);
                    Snackbar.make(view, getResources().getText(R.string.snackBarRespuestaCorrectaVacio), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else if (respuestaIncorrecta1.getText().toString().isEmpty()) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(crear.getWindowToken(), 0);
                    Snackbar.make(view, getResources().getText(R.string.snackBarRespuestaIncorrecta1Vacio), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else if (respuestaIncorrecta2.getText().toString().isEmpty()) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(crear.getWindowToken(), 0);
                    Snackbar.make(view, getResources().getText(R.string.snackBarRespuestaIncorrecta2Vacio), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else if (respuestaIncorrecta3.getText().toString().isEmpty()) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(crear.getWindowToken(), 0);
                    Snackbar.make(view, getResources().getText(R.string.snackBarRespuestaIncorrecta3Vacio), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                } else {

                    Intent startingIntent = getIntent();
                    if (startingIntent.hasExtra("id")) {

                        int idRecuperada = getIntent().getExtras().getInt("id");

                        turnBase64(imagen);

                        Pregunta preguntaRecuperada = new Pregunta(titulo.getText().toString(), spinnerCategoria.getSelectedItem().toString(), respuestaCorrecta.getText().toString(), respuestaIncorrecta1.getText().toString(), respuestaIncorrecta2.getText().toString(), respuestaIncorrecta3.getText().toString(), imagen64);
                        Repositorio.modificarPregunta(preguntaRecuperada, myContext, idRecuperada);
                        finish();

                    } else {

                        String imagen64 = turnBase64(imagen);

                        Pregunta p = new Pregunta(titulo.getText().toString(), spinnerCategoria.getSelectedItem().toString(), respuestaCorrecta.getText().toString(), respuestaIncorrecta1.getText().toString(), respuestaIncorrecta2.getText().toString(), respuestaIncorrecta3.getText().toString(), imagen64);
                        if (Repositorio.insertarPregunta(p, myContext)) {
                            Toast.makeText(myContext, "Pregunta creada correctamente", Toast.LENGTH_SHORT).show();
                            finish();

                        } else {
                            Toast.makeText(myContext, "No se ha podido crear la pregunta", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            }
        });

        borrarImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imagen.setImageDrawable(null);
            }
        });

    }

    private void takePicture() {

        try {
            // Se crea el directorio para las fotografías
            File dirFotos = new File(pathFotos);
            dirFotos.mkdirs();

            // Se crea el archivo para almacenar la fotografía
            File fileFoto = File.createTempFile(getFileCode(), ".jpg", dirFotos);

            // Se crea el objeto Uri a partir del archivo
            // A partir de la API 24 se debe utilizar FileProvider para proteger
            // con permisos los archivos creados
            // Con estas funciones podemos evitarlo
            // https://stackoverflow.com/questions/42251634/android-os-fileuriexposedexception-file-jpg-exposed-beyond-app-through-clipdata
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            uri = Uri.fromFile(fileFoto);
            Log.d(TAG, uri.getPath().toString());

            // Se crea la comunicación con la cámara
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Se le indica dónde almacenar la fotografía
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // Se lanza la cámara y se espera su resultado
            startActivityForResult(cameraIntent, REQUEST_CAPTURE_IMAGE);

        } catch (IOException ex) {

            Log.d(TAG, "Error: " + ex);
            CoordinatorLayout coordinatorLayout = findViewById(R.id.constraintLayoutNuevaPreguntaActivity);
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, getResources().getString(R.string.error_files), Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void selectPicture(){
        // Se le pide al sistema una imagen del dispositivo
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, getResources().getString(R.string.choose_picture)),
                REQUEST_SELECT_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case (REQUEST_CAPTURE_IMAGE):
                if(resultCode == NuevaPreguntaActivity.RESULT_OK){
                    // Se carga la imagen desde un objeto URI al imageView
                    ImageView imageView = findViewById(R.id.imageViewPregunta);
                    imageView.setImageURI(uri);

                    // Se le envía un broadcast a la Galería para que se actualice
                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    mediaScanIntent.setData(uri);
                    sendBroadcast(mediaScanIntent);

                } else if (resultCode == NuevaPreguntaActivity.RESULT_CANCELED) {
                    // Se borra el archivo temporal
                    File file = new File(uri.getPath());
                    file.delete();
                }
                break;

            case (REQUEST_SELECT_IMAGE):
                if (resultCode == NuevaPreguntaActivity.RESULT_OK) {
                    // Se carga la imagen desde un objeto Bitmap
                    Uri selectedImage = data.getData();
                    String selectedPath = selectedImage.getPath();

                    if (selectedPath != null) {
                        // Se leen los bytes de la imagen
                        InputStream imageStream = null;
                        try {
                            imageStream = getContentResolver().openInputStream(selectedImage);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        // Se transformam los bytes de la imagen a un Bitmap
                        Bitmap bmp = BitmapFactory.decodeStream(imageStream);

                        // Se carga el Bitmap en el ImageView
                        ImageView imageView = findViewById(R.id.imageViewPregunta);
                        imageView.setImageBitmap(bmp);
                    }
                }
                break;
    }
}
    private String getFileCode() {
        // Se crea un código a partir de la fecha y hora
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss", java.util.Locale.getDefault());
        String date = dateFormat.format(new Date());
        // Se devuelve el código
        return "pic_" + date;
    }

    private String turnBase64(ImageView imgv){

        if(imagen.getDrawable() != null){
            Bitmap imagenBitmap = ((BitmapDrawable)imagen.getDrawable()).getBitmap();
            Bitmap resized = Bitmap.createScaledBitmap(imagenBitmap, 500, 500, true);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            resized.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            imagen64 = Base64.encodeToString(byteArray, 0);
            return imagen64;
        } else {
            imagen64 = "";
            return imagen64;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Este Override te dice si has aceptado los permisos o no
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case CODE_WRITE_EXTERNAL_STORAGE_PERMISSION_CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePicture();
                } else {
                    Snackbar.make(constraintLayoutNuevaPreguntaActivity, getResources().getString(R.string.permisoEscrituraNoAceptado), Snackbar.LENGTH_LONG)
                    .show();
                }
                break;
            case CODE_WRITE_EXTERNAL_STORAGE_PERMISSION_GALERY:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectPicture();
                } else {
                    Snackbar.make(constraintLayoutNuevaPreguntaActivity, getResources().getString(R.string.permisoEscrituraNoAceptado), Snackbar.LENGTH_LONG)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public boolean onNavigateUp() {
        // Vuelve a la pantalla de resumen
        return true;
    }


    //Este metodo sobreescribe el boton  de nuestro telefono de back
    @Override
    public void onBackPressed() {
        //Vuelve a la pantalla de inicio haciendole un finish a la actividad de acerca de
        finish();
    }

    @Override
    protected void onStart() {
        MyLog.d(TAG, "Creando onStart");
        super.onStart();
        MyLog.d(TAG, "Cerrando onStart");
    }

    @Override
    protected void onResume() {
        MyLog.d(TAG, "Creando onResume");
        super.onResume();
        MyLog.d(TAG, "Cerrando onResume");
    }

    @Override
    protected void onPause() {
        MyLog.d(TAG, "Creando onPause");
        super.onPause();
        MyLog.d(TAG, "Cerrando onPause");
    }

    @Override
    protected void onStop() {
        MyLog.d(TAG, "Creando onStop");
        super.onStop();
        MyLog.d(TAG, "Cerrando onStop");
    }

    @Override
    protected void onDestroy() {
        MyLog.d(TAG, "Creando onDestroy");
        super.onDestroy();
        MyLog.d(TAG, "Cerrando onDestroy");

    }

    @Override
    protected void onRestart() {
        MyLog.d(TAG, "Creando onRestart");
        super.onRestart();
        MyLog.d(TAG, "Cerrando onRestart");
    }

}
