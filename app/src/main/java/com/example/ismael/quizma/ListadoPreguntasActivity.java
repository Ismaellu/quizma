package com.example.ismael.quizma;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListadoPreguntasActivity extends AppCompatActivity {

    private String TAG = "ListadoPreguntasActivity";
    private ArrayList<Pregunta> itemsPregunta;
    private Context myContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_preguntas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarListadoPreguntas);

        myContext = this;

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_listado_preguntas));
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Asignar la acción necesaria. En este caso "volver atrás"
                    onBackPressed();
                }
            });
        } else {
            Log.d("SobreNosotros", "Error al cargar toolbar");
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.buttomAnadirListadoPreguntas);
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                startActivity(new Intent(ListadoPreguntasActivity.this, NuevaPreguntaActivity.class));

            }
        });
    }

    @Override
    public boolean onNavigateUp() {
        // Vuelve a la pantalla de resumen
        return true;
    }



    //Este metodo sobreescribe el boton  de nuestro telefono de back
    @Override
    public void onBackPressed() {
        //Vuelve a la pantalla de inicio haciendole un finish a la actividad de acerca de
        finish();
    }

    @Override
    protected void onStart() {
        MyLog.d(TAG, "Creando onStart");
        super.onStart();
        MyLog.d(TAG, "Cerrando onStart");
    }

    @Override
    protected void onResume() {
        MyLog.d(TAG, "Creando onResume");
        super.onResume();

        TextView ocultarText = (TextView) findViewById(R.id.textViewListadoPreguntas);

        ocultarText.setVisibility(View.VISIBLE);

        if (Repositorio.consultarBD(myContext) != null) {

            ocultarText.setVisibility(View.GONE);

            itemsPregunta = Repositorio.consultarBD(myContext);
            final RecyclerView recyclerViewListado = (RecyclerView) findViewById(R.id.recyclerViewListado);

            // Crea el Adaptador con los datos de la lista anterior
            PreguntaAdapter adaptador = new PreguntaAdapter(itemsPregunta);

            // Asocia el elemento de la lista con una acción al ser pulsado
            adaptador.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = recyclerViewListado.getChildAdapterPosition(v);


                    /**
                     * Intent it = new Intent(ListadoPreguntasActivity.this, NuevaPreguntaActivity.class);
                     * it.putExtra("idTransferido", itemsPregunta.get(position).getId());
                     * startActivity(it);
                     */

                    Intent it = new Intent(ListadoPreguntasActivity.this, NuevaPreguntaActivity.class);
                    //Bundle b = new Bundle();
                    //b.putInt("id", itemsPregunta.get(position).getId());
                    it.putExtra("id", itemsPregunta.get(position).getId());
                    startActivity(it);

                }
            });

            // Asocia el Adaptador al RecyclerView
            recyclerViewListado.setAdapter(adaptador);

            // Muestra el RecyclerView en vertical
            recyclerViewListado.setLayoutManager(new LinearLayoutManager(myContext));

            MyLog.d(TAG, "Cerrando onResume");
        }
    }

    @Override
    protected void onPause() {
        MyLog.d(TAG, "Creando onPause");
        super.onPause();
        MyLog.d(TAG, "Cerrando onPause");
    }

    @Override
    protected void onStop() {
        MyLog.d(TAG, "Creando onStop");
        super.onStop();
        MyLog.d(TAG, "Cerrando onStop");
    }

    @Override
    protected void onDestroy() {
        MyLog.d(TAG, "Creando onDestroy");
        super.onDestroy();
        MyLog.d(TAG, "Cerrando onDestroy");

    }

    @Override
    protected void onRestart() {
        MyLog.d(TAG, "Creando onRestart");
        super.onRestart();
        MyLog.d(TAG, "Cerrando onRestart");
    }



}
