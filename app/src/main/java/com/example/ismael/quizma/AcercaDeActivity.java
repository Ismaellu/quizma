package com.example.ismael.quizma;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

public class AcercaDeActivity extends AppCompatActivity {

    private String TAG = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acerca_de);
        //Esto que hay encima tiene que estar siempre
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarAcercaDe);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_acerca_de));
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Asignar la acción necesaria. En este caso "volver atrás"
                    onBackPressed();
                }
            });
        } else {
            Log.d("SobreNosotros", "Error al cargar toolbar");
        }
    }

    @Override
    public boolean onNavigateUp() {
        // Vuelve a la pantalla de resumen
        return true;
    }



    //Este metodo sobreescribe el boton  de nuestro telefono de back
    @Override
    public void onBackPressed() {
        //Vuelve a la pantalla de inicio haciendole un finish a la actividad de acerca de
        finish();
    }

    @Override
    protected void onStart() {
        MyLog.d(TAG, "Creando onStart");
        super.onStart();
        MyLog.d(TAG, "Cerrando onStart");
    }

    @Override
    protected void onResume() {
        MyLog.d(TAG, "Creando onResume");
        super.onResume();
        MyLog.d(TAG, "Cerrando onResume");
    }

    @Override
    protected void onPause() {
        MyLog.d(TAG, "Creando onPause");
        super.onPause();
        MyLog.d(TAG, "Cerrando onPause");
    }

    @Override
    protected void onStop() {
        MyLog.d(TAG, "Creando onStop");
        super.onStop();
        MyLog.d(TAG, "Cerrando onStop");
    }

    @Override
    protected void onDestroy() {
        MyLog.d(TAG, "Creando onDestroy");
        super.onDestroy();
        MyLog.d(TAG, "Cerrando onDestroy");

    }

    @Override
    protected void onRestart() {
        MyLog.d(TAG, "Creando onRestart");
        super.onRestart();
        MyLog.d(TAG, "Cerrando onRestart");
    }

}
