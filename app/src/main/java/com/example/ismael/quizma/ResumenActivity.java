package com.example.ismael.quizma;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ResumenActivity extends AppCompatActivity {

    private String TAG = "ResumenActivity";
    private Context myContext;
    private ConstraintLayout constraintLayoutResumenActivity;
    private ImageView imagenGato;
    private Button jugarGato;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resumen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarResumen);
        setSupportActionBar(toolbar);
        myContext = this;
        imagenGato = (ImageView) findViewById(R.id.imageViewGato);
        jugarGato = (Button) findViewById(R.id.buttonTransaccion);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.buttomAnadirResumen);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        jugarGato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Metodos.trans(myContext, imagenGato);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_resumen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_acerca_de:
                MyLog.i("ActionBar", "Abriendo Acerca De");
                //Esto sirve para hacer un start activity del acerca de
                startActivity(new Intent(ResumenActivity.this, AcercaDeActivity.class));
                return true;
            case R.id.action_configuracion:
                MyLog.i("ActionBar", "Configuración");
                return true;
            case R.id.action_listadoPreguntas:
                MyLog.i("ActionBar", "Listado de Preguntas");
                startActivity(new Intent(ResumenActivity.this, ListadoPreguntasActivity.class));
                return true;
            case R.id.action_salir:
                MyLog.i("ActionBar", "Salir");

                return true;
            case R.id.action_exportar:

                int WriteExternalStoragePermission = ContextCompat.checkSelfPermission(myContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                MyLog.d("ModificacionPreguntasActivity", "WRITE_EXTERNAL_STORAGE Permission: " + WriteExternalStoragePermission);


                if (WriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
                    // Permiso denegado
                    // A partir de Marshmallow (6.0) se pide aceptar o rechazar el permiso en tiempo de ejecución
                    // En las versiones anteriores no es posibse ejecuta el método "onRequestPermissionsResult" para manejar la respuesta
                    //                            // Si el usuario marca "No preguntar más" no se volverá a mle hacerlo
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        ActivityCompat.requestPermissions(ResumenActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WriteExternalStoragePermission);
                        // Una vez que se pide aceptar o rechazar el permiso mostrar este diálogo
                    } else {
                        Snackbar.make(constraintLayoutResumenActivity, getResources().getString(R.string.permisoEscrituraGarantizado), Snackbar.LENGTH_LONG)
                                .show();
                    }
                } else {
                    if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
                        Intent emailIntent = Metodos.exportarXML(myContext);
                        startActivity(Intent.createChooser(emailIntent, "Exportar Preguntas"));
                    }
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onStart() {
        MyLog.d(TAG, "Creando onStart");
        super.onStart();
        MyLog.d(TAG, "Cerrando onStart");
    }

    @Override
    protected void onResume() {
        MyLog.d(TAG, "Creando onResume");
        super.onResume();
        TextView textViewNumeroCategorias = findViewById(R.id.textViewNumeroCategorias);
        TextView textViewNumeroPreguntas = findViewById(R.id.textViewNumeroPreguntas);

        textViewNumeroPreguntas  .setText(String.valueOf(Repositorio.numeroPreguntas(myContext)));
        textViewNumeroCategorias.setText(String.valueOf(Repositorio.numeroCategorias(myContext)));


        MyLog.d(TAG, "Cerrando onResume");
    }

    @Override
    protected void onPause() {
        MyLog.d(TAG, "Creando onPause");
        super.onPause();
        MyLog.d(TAG, "Cerrando onPause");
    }

    @Override
    protected void onStop() {
        MyLog.d(TAG, "Creando onStop");
        super.onStop();
        MyLog.d(TAG, "Cerrando onStop");
    }

    @Override
    protected void onDestroy() {
        MyLog.d(TAG, "Creando onDestroy");
        super.onDestroy();
        MyLog.d(TAG, "Cerrando onDestroy");

    }

    @Override
    protected void onRestart() {
        MyLog.d(TAG, "Creando onRestart");
        super.onRestart();
        MyLog.d(TAG, "Cerrando onRestart");
    }

}
