package com.example.ismael.quizma;

public class Constantes {
    public static final float VIEWPORT_WIDTH = 5.0f;
    public static final String REST_URL = "http://www.ejemplo.com/api";
    public static final String nombreDB = "DBPreguntas";
    public static final String tablaPreguntas = "Preguntas";
    public static final String idPregunta = "id";
    public static final String tituloPregunta = "titulo";
    public static final String categoriaPregunta = "categoria";
    public static final String correctaPregunta = "correcta";
    public static final String incorrecta1Pregunta = "incorrecta1";
    public static final String incorrecta2Pregunta = "incorrecta2";
    public static final String incorrecta3Pregunta = "incorrecta3";
    public static final String imagen64 = "imagen64";
    public static final String crearTabla = "CREATE TABLE " + tablaPreguntas + " ("+idPregunta+" INTEGER PRIMARY KEY AUTOINCREMENT, " +tituloPregunta+ " TEXT, "+categoriaPregunta+" TEXT, "+correctaPregunta+" TEXT, "+incorrecta1Pregunta+" TEXT, "+incorrecta2Pregunta+" TEXT, "+incorrecta3Pregunta+" TEXT, "+imagen64+" TEXT)";
    public static final String consultarTabla = " SELECT * FROM " + tablaPreguntas + " ORDER BY "+tituloPregunta;
    public static final String consultarCategoria = " SELECT DISTINCT "+categoriaPregunta+" FROM Preguntas ORDER BY "+categoriaPregunta;
    public static final String recuperarPreguntaModificar = "SELECT * FROM " + tablaPreguntas + " WHERE " + idPregunta + " = ?";
    public static final String numeroCategorias = "SELECT COUNT (DISTINCT "+categoriaPregunta+") FROM "+tablaPreguntas;
    public static final String numeroPreguntas = "SELECT COUNT (DISTINCT "+idPregunta+") FROM "+tablaPreguntas;

}
