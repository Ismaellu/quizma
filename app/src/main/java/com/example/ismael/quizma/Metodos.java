package com.example.ismael.quizma;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import java.io.File;
import java.io.FileWriter;

public class Metodos {

    @SuppressWarnings("null")

    public static Intent exportarXML(Context contexto){
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/preguntasExportar");
        String fname = "preguntas.xml";
        File file = new File (myDir, fname);
        try
        {
            if (!myDir.exists()) {
                myDir.mkdirs();
            }
            if (file.exists ())
                file.delete ();
            FileWriter fw=new FileWriter(file);
            //Escribimos en el fichero un String
            fw.write(Repositorio.CreateXMLString(contexto));
            //Cierro el stream
            fw.close();
        }
        catch (Exception ex)
        {
            MyLog.e("Ficheros", "Error al escribir fichero a memoria interna");
        }
        String cadena = myDir.getAbsolutePath()+"/"+fname;
        Uri path = Uri.parse("file://"+cadena);

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto","ailuna@iesfranciscodelosrios.es", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Preguntas Exportadas");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Preguntas exportadas de la aplicacion Quizma");
        emailIntent .putExtra(Intent.EXTRA_STREAM, path);
        return emailIntent;
    }

    public static void trans(Context myContext, ImageView imagenGato){
        Animation animation = AnimationUtils.loadAnimation(myContext,R.anim.trans);
        imagenGato.startAnimation(animation);
    }
}
