package com.example.ismael.quizma;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Environment;
import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;

public class Repositorio {


    /**
     * Metodos insertar actualizar y borrar, publicos staticos
     */

    public static boolean insertarPregunta(Pregunta p, Context contexto) {
        //Abrimos la base de datos 'DBPreguntas' en modo escritura
        BaseDeDatosSQLiteHelper baseDatosHelper = new BaseDeDatosSQLiteHelper(contexto, Constantes.nombreDB, null, 1);

        SQLiteDatabase db = baseDatosHelper.getWritableDatabase();

        //Si hemos abierto correctamente la base de datos
        if (db != null) {

            try {
                ContentValues values = new ContentValues();
                values.put(Constantes.tituloPregunta, p.getTitulo());
                values.put(Constantes.categoriaPregunta, p.getCategoria());
                values.put(Constantes.correctaPregunta, p.getRespuestaCorrecta());
                values.put(Constantes.incorrecta1Pregunta, p.getRepuestaIncorrecta1());
                values.put(Constantes.incorrecta2Pregunta, p.getRepuestaIncorrecta2());
                values.put(Constantes.incorrecta3Pregunta, p.getRepuestaIncorrecta3());
                values.put(Constantes.imagen64, p.getImagen64());

                //Insertar un registro

                db.insert(Constantes.tablaPreguntas, null, values);
            } catch (SQLiteException e) {
                MyLog.d("Repositorio", "Ha fallado el insert");
                return false;
            }

            //Cerramos la base de datos
            db.close();
            MyLog.d("Repositorio", "Se ha insertado correctamente");
            return true;
        } else {
            return false;
        }
    }

    public static ArrayList<Pregunta> consultarBD(Context contexto) {

        ArrayList<Pregunta> itemsPregunta = new ArrayList<Pregunta>();
        BaseDeDatosSQLiteHelper baseDatosHelper = new BaseDeDatosSQLiteHelper(contexto, Constantes.nombreDB, null, 1);

        SQLiteDatabase db = baseDatosHelper.getWritableDatabase();

        Cursor c = db.rawQuery(Constantes.consultarTabla, null);


        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    //Asignamos el valor en nuestras variables para usarlos en lo que necesitemos
                    int idCursor = c.getInt(c.getColumnIndex(Constantes.idPregunta));
                    String tituloCursor = c.getString(c.getColumnIndex(Constantes.tituloPregunta));
                    String categoriaCursor = c.getString(c.getColumnIndex(Constantes.categoriaPregunta));
                    String correctaCursor = c.getString(c.getColumnIndex(Constantes.correctaPregunta));
                    String incorrecta1Cursor = c.getString(c.getColumnIndex(Constantes.incorrecta1Pregunta));
                    String incorrecta2Cursor = c.getString(c.getColumnIndex(Constantes.incorrecta2Pregunta));
                    String incorrecta3Cursor = c.getString(c.getColumnIndex(Constantes.incorrecta3Pregunta));
                    String imagen64 = c.getString(c.getColumnIndex(Constantes.imagen64));

                    itemsPregunta.add(new Pregunta(idCursor, tituloCursor, categoriaCursor, correctaCursor, incorrecta1Cursor, incorrecta2Cursor, incorrecta3Cursor, imagen64));

                } while (c.moveToNext());
            }
        }

        c.close();
        db.close();

        return itemsPregunta;

    }

    public static Pregunta recuperarPregunta(int idRecuperado, Context contexto) {

        BaseDeDatosSQLiteHelper baseDatosHelper = new BaseDeDatosSQLiteHelper(contexto, Constantes.nombreDB, null, 1);

        SQLiteDatabase db = baseDatosHelper.getWritableDatabase();

        String[] args = new String[] {Integer.toString(idRecuperado)};
        Cursor c = db.rawQuery(Constantes.recuperarPreguntaModificar, args);

        if (c != null) {
            if (c.moveToFirst()) {

                String tituloCursor = c.getString(c.getColumnIndex(Constantes.tituloPregunta));
                String categoriaCursor = c.getString(c.getColumnIndex(Constantes.categoriaPregunta));
                String correctaCursor = c.getString(c.getColumnIndex(Constantes.correctaPregunta));
                String incorrecta1Cursor = c.getString(c.getColumnIndex(Constantes.incorrecta1Pregunta));
                String incorrecta2Cursor = c.getString(c.getColumnIndex(Constantes.incorrecta2Pregunta));
                String incorrecta3Cursor = c.getString(c.getColumnIndex(Constantes.incorrecta3Pregunta));
                int idCursor = c.getInt(c.getColumnIndex(Constantes.idPregunta));
                String imagen64 = c.getString(c.getColumnIndex(Constantes.imagen64));

                Pregunta p = new Pregunta(idCursor, tituloCursor, categoriaCursor, correctaCursor, incorrecta1Cursor, incorrecta2Cursor, incorrecta3Cursor, imagen64);

                return p;
            }
        }

        c.close();
        db.close();
        return null;

    }

    public static ArrayList<String> consultarCategoria(Context contexto) {

        ArrayList<String> itemsCategoria = new ArrayList<String>();
        BaseDeDatosSQLiteHelper baseDatosHelper = new BaseDeDatosSQLiteHelper(contexto, Constantes.nombreDB, null, 1);

        SQLiteDatabase db = baseDatosHelper.getWritableDatabase();

        Cursor c = db.rawQuery(Constantes.consultarCategoria, null);


        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    //Asignamos el valor en nuestras variables para usarlos en lo que necesitemos
                    String categoriaCursor = c.getString(c.getColumnIndex(Constantes.categoriaPregunta));
                    itemsCategoria.add(categoriaCursor);

                } while (c.moveToNext());
            }
        }

        c.close();
        db.close();

        return itemsCategoria;

    }

    public static boolean modificarPregunta (Pregunta p, Context contexto, int idRecuperado) {
        //Abrimos la base de datos 'DBPreguntas' en modo escritura
        BaseDeDatosSQLiteHelper baseDatosHelper = new BaseDeDatosSQLiteHelper(contexto, Constantes.nombreDB, null, 1);

        SQLiteDatabase db = baseDatosHelper.getWritableDatabase();

        //Si hemos abierto correctamente la base de datos
        if (db != null) {

            try {
                ContentValues values = new ContentValues();
                values.put(Constantes.tituloPregunta, p.getTitulo());
                values.put(Constantes.categoriaPregunta, p.getCategoria());
                values.put(Constantes.correctaPregunta, p.getRespuestaCorrecta());
                values.put(Constantes.incorrecta1Pregunta, p.getRepuestaIncorrecta1());
                values.put(Constantes.incorrecta2Pregunta, p.getRepuestaIncorrecta2());
                values.put(Constantes.incorrecta3Pregunta, p.getRepuestaIncorrecta3());
                values.put(Constantes.imagen64, p.getImagen64());

                String[] args = new String[] {Integer.toString(idRecuperado)};
                //Modificar un registro
                db.update(Constantes.tablaPreguntas, values, "id=?", args);
            } catch (SQLiteException e) {
                MyLog.d("Repositorio", "Ha fallado el modificar");
                return false;
            }
            //Cerramos la base de datos
            db.close();
            MyLog.d("Repositorio", "Se ha modificado correctamente");
            return true;
        } else {
            return false;
        }
    }

    public static int numeroCategorias (Context contexto){
        //Abrimos la base de datos 'DBPreguntas' en modo escritura
        BaseDeDatosSQLiteHelper baseDatosHelper = new BaseDeDatosSQLiteHelper(contexto, Constantes.nombreDB, null, 1);

        SQLiteDatabase db = baseDatosHelper.getWritableDatabase();

        int contadorCategorias = 0;

        //Si hemos abierto correctamente la base de datos
        if (db != null) {

            Cursor c = db.rawQuery(Constantes.numeroCategorias, null);

            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        //Asignamos el valor en nuestras variables para usarlos en lo que necesitemos
                        contadorCategorias = c.getInt(0);
                    } while (c.moveToNext());
                }
            }

        }
        return contadorCategorias;
    }

    public static int numeroPreguntas (Context contexto){

        int contadorPreguntas = 0;
        //Abrimos la base de datos 'DBPreguntas' en modo escritura
        BaseDeDatosSQLiteHelper baseDatosHelper = new BaseDeDatosSQLiteHelper(contexto, Constantes.nombreDB, null, 1);

        SQLiteDatabase db = baseDatosHelper.getWritableDatabase();



        //Si hemos abierto correctamente la base de datos
        if (db != null) {

            Cursor c = db.rawQuery(Constantes.numeroCategorias, null);

            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        //Asignamos el valor en nuestras variables para usarlos en lo que necesitemos
                        contadorPreguntas = c.getInt(0);
                    } while (c.moveToNext());
                }
            }

        }
        return contadorPreguntas;
    }

    public static boolean eliminarPregunta(Context contexto, int idRecogido) {

        //Abrimos la base de datos 'DBPreguntas' en modo escritura
        BaseDeDatosSQLiteHelper baseDatosHelper = new BaseDeDatosSQLiteHelper(contexto, Constantes.nombreDB, null, 1);

        SQLiteDatabase db = baseDatosHelper.getWritableDatabase();

        //si la conexion es exitosa
        if (db != null) {

            //Insertamos los datos en la tabla Pregunta
            try {
                String[] args = new String[] {Integer.toString(idRecogido)};
                db.delete(Constantes.tablaPreguntas,"id = ?", args);

            } catch (SQLiteException e) {
                MyLog.d("Repositorio", "Ha fallado el borrar");
                return false;
            }
            //Cerramos la base de datos
            db.close();

            return true;

        } else {

            return false;

        }
    }

    public static String CreateXMLString(Context contexto) throws IllegalArgumentException, IllegalStateException, IOException
    {
        ArrayList<Pregunta> preguntasXML = new ArrayList<Pregunta>();
        preguntasXML= consultarBD(contexto);


        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        xmlSerializer.setOutput(writer);

        //Start Document
        xmlSerializer.startDocument("UTF-8", true);
        xmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

        //Open Tag <file>
        xmlSerializer.startTag("", "quiz");

        for (Pregunta p: preguntasXML) {
            //Categoria de cada pregunta
            xmlSerializer.startTag("", "question");
            xmlSerializer.attribute("", "type", p.getCategoria());
            xmlSerializer.startTag("", "category");
            xmlSerializer.text(p.getCategoria());
            xmlSerializer.endTag("", "category");
            xmlSerializer.endTag("", "question");
            //Pregunta de eleccion multiple
            xmlSerializer.startTag("", "question");
            xmlSerializer.attribute("", "type", "multichoice");
            xmlSerializer.startTag("", "name");
            xmlSerializer.text(p.getTitulo());
            xmlSerializer.endTag("", "name");
            xmlSerializer.startTag("","questiontext");
            xmlSerializer.attribute("", "format", "html");
            xmlSerializer.text(p.getTitulo());
            xmlSerializer.startTag("","file");
            xmlSerializer.attribute("", "name", p.getImagen64());
            xmlSerializer.attribute("", "path", "/");
            xmlSerializer.attribute("", "encoding", "base64");
            xmlSerializer.endTag("", "file");
            xmlSerializer.endTag("", "questiontext");
            xmlSerializer.startTag("","answernumbering");
            xmlSerializer.endTag("", "answernumbering");
            xmlSerializer.startTag("","answer");
            xmlSerializer.attribute("","fraction", "100");
            xmlSerializer.attribute("", "format", "html");
            xmlSerializer.text(p.getRespuestaCorrecta());
            xmlSerializer.endTag("", "answer");
            xmlSerializer.startTag("","answer");
            xmlSerializer.attribute("","fraction", "0");
            xmlSerializer.attribute("", "format", "html");
            xmlSerializer.text(p.getRepuestaIncorrecta1());
            xmlSerializer.endTag("", "answer");
            xmlSerializer.startTag("","answer");
            xmlSerializer.attribute("","fraction", "0");
            xmlSerializer.attribute("", "format", "html");
            xmlSerializer.text(p.getRepuestaIncorrecta2());
            xmlSerializer.endTag("", "answer");
            xmlSerializer.startTag("","answer");
            xmlSerializer.attribute("","fraction", "0");
            xmlSerializer.attribute("", "format", "html");
            xmlSerializer.text(p.getRepuestaIncorrecta3());
            xmlSerializer.endTag("", "answer");
            xmlSerializer.endTag("","question");
        }

        //end tag <file>
        xmlSerializer.endTag("","quiz");
        xmlSerializer.endDocument();
        return writer.toString();


    }




}
