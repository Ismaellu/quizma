package com.example.ismael.quizma;

public class Pregunta {

    private int id;
    private String titulo;
    private String categoria;
    private String respuestaCorrecta;
    private String repuestaIncorrecta1;
    private String repuestaIncorrecta2;
    private String repuestaIncorrecta3;
    private String imagen64;

    public Pregunta(String titulo, String categoria, String respuestaCorrecta, String repuestaIncorrecta1, String repuestaIncorrecta2, String repuestaIncorrecta3, String imagen64) {
        this.titulo = titulo;
        this.categoria = categoria;
        this.respuestaCorrecta = respuestaCorrecta;
        this.repuestaIncorrecta1 = repuestaIncorrecta1;
        this.repuestaIncorrecta2 = repuestaIncorrecta2;
        this.repuestaIncorrecta3 = repuestaIncorrecta3;
        this.imagen64 = imagen64;
    }

    public Pregunta(int id, String titulo, String categoria, String respuestaCorrecta, String repuestaIncorrecta1, String repuestaIncorrecta2, String repuestaIncorrecta3, String imagen64){
        this.id = id;
        this.titulo = titulo;
        this.categoria = categoria;
        this.respuestaCorrecta = respuestaCorrecta;
        this.repuestaIncorrecta1 = repuestaIncorrecta1;
        this.repuestaIncorrecta2 = repuestaIncorrecta2;
        this.repuestaIncorrecta3 = repuestaIncorrecta3;
        this.imagen64 = imagen64;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getRespuestaCorrecta() {
        return respuestaCorrecta;
    }

    public void setRespuestaCorrecta(String respuestaCorrecta) {
        this.respuestaCorrecta = respuestaCorrecta;
    }

    public String getRepuestaIncorrecta1() {
        return repuestaIncorrecta1;
    }

    public void setRepuestaIncorrecta1(String repuestaIncorrecta1) {
        this.repuestaIncorrecta1 = repuestaIncorrecta1;
    }

    public String getRepuestaIncorrecta2() {
        return repuestaIncorrecta2;
    }

    public void setRepuestaIncorrecta2(String repuestaIncorrecta2) {
        this.repuestaIncorrecta2 = repuestaIncorrecta2;
    }

    public String getRepuestaIncorrecta3() {
        return repuestaIncorrecta3;
    }

    public void setRepuestaIncorrecta3(String repuestaIncorrecta3) {
        this.repuestaIncorrecta3 = repuestaIncorrecta3;
    }

    public String getImagen64() {
        return imagen64;
    }

    public void setImagen64(String imagen64) {
        this.imagen64 = imagen64;
    }
}
