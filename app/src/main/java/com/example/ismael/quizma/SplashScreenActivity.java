package com.example.ismael.quizma;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreenActivity extends AppCompatActivity {

    private String TAG = "SplashScreenActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MyLog.d(TAG, "Creando onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        //Aqui empieza el comntador de segundos para el splashscreen
        int secondsDelayed = 5;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                //En el primer activity hay que poner el nombre de la actividad en la que esta y en el segundo la actividad
                //a la que quieres que te lleve
                startActivity(new Intent(SplashScreenActivity.this, ResumenActivity.class));
                finish();
            }
            //Aqui se ponen los milisegundos que tarda
        }, secondsDelayed * 1000);
        MyLog.d(TAG, "Cerrando onCreate");
    }

    @Override
    protected void onStart() {
        MyLog.d(TAG, "Creando onStart");
        super.onStart();
        MyLog.d(TAG, "Cerrando onStart");
    }

    @Override
    protected void onResume() {
        MyLog.d(TAG, "Creando onResume");
        super.onResume();
        MyLog.d(TAG, "Cerrando onResume");
    }

    @Override
    protected void onPause() {
        MyLog.d(TAG, "Creando onPause");
        super.onPause();
        MyLog.d(TAG, "Cerrando onPause");
    }

    @Override
    protected void onStop() {
        MyLog.d(TAG, "Creando onStop");
        super.onStop();
        MyLog.d(TAG, "Cerrando onStop");
    }

    @Override
    protected void onDestroy() {
        MyLog.d(TAG, "Creando onDestroy");
        super.onDestroy();
        MyLog.d(TAG, "Cerrando onDestroy");

    }

    @Override
    protected void onRestart() {
        MyLog.d(TAG, "Creando onRestart");
        super.onRestart();
        MyLog.d(TAG, "Cerrando onRestart");
    }









}
